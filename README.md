### Read me

Simulátor umožnuje vytvorenie premenných, inštrukcií na zmenu premenných a vizualizáciu hodnôt.  
Premenná v zápise sa nahradí jej hodnotou a následne vypočíta pomocou [https://am.angouri.org/](https://am.angouri.org/).

*   V zápise je možné použiváť matematické operácie (`sin( x )`).
*   Pri inštrukciách je možné zadať podmienku pre vykonanie, výsledok musí byť **boolean** `x < y or ( var1 = var2 ) and ( var3 = 0 )` (jedno `=` pre porovnanie)
*   Použitá premenná v grafe alebo v inštrukcií musí byť odelená medzerami `var1 + var2 * ( x / y ) * sin( x )`
*   V záložke **import / export** je možné uložiť aktuálny stav do pamäte prehliadača / obnoviť
*   Hodnota Key v Pie chart môže byť **string** iba v prípade že začína úvodzovkami `"Text input`
*   Náhodné číslo je možné zadať zápisom `rand( min, max )`

##### Chyby

*   Vstupy nemajú validáciu
*   Pozor na zacyklenie, maximálna hĺbka výpočtu nie je stanovená! `(x = y + 2` a `y = x / 2)`
*   ...

##### Todo

*   Obnovenie stavu priamo z json
*   Editáciu stavu v json
*   Editácia / reset grafu
*   Viac možností vizualizácií
*   Condition watcher
*   Ukladať stav priebežne
