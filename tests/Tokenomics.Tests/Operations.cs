using Tokenomics.Designer.Operations;

namespace Tokenomics.Tests
{
    public class Operations
    {
        [Fact]
        public void TestRandom()
        {
            var randOp = new RandomOperation();
            var input = "10 + rand(0, 100) + rand( 1, 40 )";
            var output1 = randOp.Process(input);
            var output2 = randOp.Process(input);
            Assert.NotEqual(input, output1);
            Assert.NotEqual(output1, output2);
        }
    }
}