﻿using System.Globalization;

namespace Tokenomics.Designer.Extensions
{
    public static class StringExtensions
    {
        public static string Tokenized(this string value) => $" {value.Trim().Replace(",", ".")} ";
        public static string Tokenized(this double? value) => Tokenized(value.ToString());
        public static string Tokenized(this double value) => Tokenized(value.ToString());
    }
}
