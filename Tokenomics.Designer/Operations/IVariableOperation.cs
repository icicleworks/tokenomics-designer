﻿namespace Tokenomics.Designer.Operations
{
    public interface IVariableOperation
    {
        string Process(string input);
    }
}
