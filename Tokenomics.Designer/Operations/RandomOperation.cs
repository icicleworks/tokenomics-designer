﻿using System.Text.RegularExpressions;

namespace Tokenomics.Designer.Operations
{
    public class RandomOperation : IVariableOperation
    {
        Random Random = new Random();

        public string Process(string input)
        {
            Regex randRgx = new(@"rand\(.{0,2}\d{1,}.\d{1,}.{0,2}\)");
            Regex numRgx = new(@"((\d{1,}\.\d{1,})|\d{1,})");

            var randoms = randRgx.Matches(input);
            foreach (Match randomsMatch in randoms.Reverse())
            {
                var nums = numRgx.Matches(randomsMatch.Value);
                var min = int.Parse(nums[0].Value);
                var max = int.Parse(nums[1].Value) + 1;
                input = input.Remove(randomsMatch.Index, randomsMatch.Length);
                var rnd = Random.Next(min, max);
                input = input.Insert(randomsMatch.Index, rnd.ToString());
            }

            return input;
        }
    }
}
