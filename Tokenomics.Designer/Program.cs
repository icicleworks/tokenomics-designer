using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Tokenomics.Designer;
using Tokenomics.Designer.Events;
using Tokenomics.Designer.Operations;
using Tokenomics.Designer.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddBlazoredLocalStorage();

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddSingleton<VariablesManager>();
builder.Services.AddSingleton<ChartsManager>();
builder.Services.AddSingleton<SimulatorManager>();
builder.Services.AddSingleton<VariableEvents>();
builder.Services.AddSingleton<ChartEvents>();
builder.Services.AddScoped<BackupManager>();

builder.Services.AddSingleton<RandomOperation>();

var app = builder.Build();

//await app.Services.GetRequiredService<BackupManager>().LoadScene("current", true);

await app.RunAsync();
