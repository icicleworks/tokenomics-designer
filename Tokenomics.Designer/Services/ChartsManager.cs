﻿using Tokenomics.Designer.Events;
using Tokenomics.Designer.Extensions;
using Tokenomics.Designer.Models;

namespace Tokenomics.Designer.Services
{
    public class ChartsManager
    {
        public readonly List<BaseChart> Charts = new();
        private readonly ILogger<ChartsManager> _logger;
        private readonly VariablesManager _variablesManager;
        private readonly ChartEvents _chartEvents;

        public ChartsManager(ILogger<ChartsManager> logger, VariablesManager variablesManager, VariableEvents variableEvents, ChartEvents chartEvents)
        {
            this._logger = logger;
            _variablesManager = variablesManager;
            this._chartEvents = chartEvents;
            variableEvents.VariablesUpdated += OnVariablesUpdated;
        }

        public void AddChart(BaseChart chart)
        {

            if (Charts.Any(c => c.Name == chart.Name))
            {
                var existing = Charts.First(c => c.Name == chart.Name);
                existing.Keys.AddRange(chart.Keys);
                existing.Series.AddRange(chart.Series);
                existing.Values.AddRange(chart.Values);

                _logger.LogInformation("Adding to existing chart {Chart}", chart);
            } else
            {
                _logger.LogInformation("Adding new chart {Chart}", chart);
                Charts.Add(chart);
            }
            _chartEvents.OnChartsUpdated();
        }

        public void RemoveChart(BaseChart chart)
        {
            Charts.Remove(chart);
            _chartEvents.OnChartsUpdated();
        }

        public void ResetCharts()
        {
           Charts.ForEach(chart =>
           {
               chart.ResetValues();
           });
        }

        public void OnVariablesUpdated(object? _, EventArgs __)
        {
            Charts.ForEach(async chart =>
            {
                for(int i = 0; i < chart.Keys.Count; i++)
                {
                    var key = chart.Keys[i].Tokenized();
                    object x = key.ToString().StartsWith(" \"") ? key.Replace(" \"", "") : await _variablesManager.ResolveVariableStringValue(key);
                    var value = await _variablesManager.ResolveVariableStringValue(chart.GetFxString(i, x is double ? x as double? : null));
                    chart.AddValue(i, new(x, value));
                }
            });
            Console.WriteLine("updating charts");
            _chartEvents.OnChartsUpdated();
        }
    }
}
