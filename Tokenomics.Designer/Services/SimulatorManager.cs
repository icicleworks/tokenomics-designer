﻿using AngouriMath.Extensions;
using Tokenomics.Designer.Extensions;
using Tokenomics.Designer.Models;

namespace Tokenomics.Designer.Services
{
    public class SimulatorManager
    {
        private readonly ILogger<SimulatorFunction> _logger;
        private readonly VariablesManager _variablesManager;
        private readonly ChartsManager _chartsManager;

        public List<SimulatorFunction> Functions { get; set; } = new() { new() { Name = "First"} };

        public SimulatorSettings Settings { get; set; } = new();

        public SimulatorManager(ILogger<SimulatorFunction> logger, VariablesManager variablesManager, ChartsManager chartsManager)
        {
            this._logger = logger;
            this._variablesManager = variablesManager;
            this._chartsManager = chartsManager;
        }

        public async Task Run(CancellationToken? cancellationToken = null)
        {
            Reset();
            var tf = new TaskFactory();
            for (; Settings.Iteration < Settings.Iterations; Settings.Iteration += Settings.IterateBy)
            {

                if (cancellationToken?.IsCancellationRequested ?? false)
                {
                    return;
                }
                _logger.LogInformation("Simulator iteration {Iteration}/{Iterations}", Settings.Iteration, Settings.Iterations);

                await tf.StartNew(async () =>
                {
                    foreach (var simulatorFunction in Functions.Where(f => f.Enabled))
                    {
                        _logger.LogInformation("Running function: {SimulatorFunctionName}", simulatorFunction.Name);
                        foreach (var instruction in simulatorFunction.Instructions)
                        {
                            if (!string.IsNullOrEmpty(instruction.Condition) && !(await EvaluateCondition(instruction.Condition)))
                            {
                                continue;
                            }

                            _logger.LogInformation("Updating variable {VariableName} with {Instruction}", instruction.VariableName, instruction.GetInstruction(Settings.Iteration));
                            var variable = _variablesManager.GetVariableByName(instruction.VariableName);
                            var newValue = await _variablesManager.ResolveVariableStringValue(instruction.GetInstruction(Settings.Iteration));
                            var oldValue = await _variablesManager.GetValue(variable.Name);
                            await _variablesManager.UpdateVariable(variable, $"{newValue}", true);
                        }
                        await _variablesManager.ResolveVariables();
                    }
                });
                await Task.Delay(Settings.IterationTime);
            }
        }

        public async Task<bool> EvaluateCondition(string condition)
        {
            var pCondition = await _variablesManager.ProcessInputString(condition.Tokenized());
            var results = pCondition.EvalBoolean().Value;

            Console.WriteLine("Condition results = " + results);

            return results;
        }

        public void Reset()
        {
            Settings.Iteration = 0;
        }
    }
}
