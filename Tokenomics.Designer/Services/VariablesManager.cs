﻿using AngouriMath;
using Tokenomics.Designer.Events;
using Tokenomics.Designer.Extensions;
using Tokenomics.Designer.Operations;
using static AngouriMath.Entity;

namespace Tokenomics.Designer.Services
{
    public class VariablesManager
    {
        public readonly Dictionary<Models.Variable, double?> Variables = new();
        private readonly ILogger<VariablesManager> logger;
        private readonly VariableEvents _variableEvents;
        private readonly RandomOperation randomOperation;

        public VariablesManager(ILogger<VariablesManager> logger, VariableEvents variableEvents, RandomOperation randomOperation)
        {
            this.logger = logger;
            this._variableEvents = variableEvents;
            this.randomOperation = randomOperation;

            // Time variable for simulator
            AddVariable(new()
            {
                Name = "t",
                Value = "0"
            });
        }

        public async Task AddVariable(Models.Variable variable)
        {
            logger.LogInformation("Adding variable");
            Variables.TryAdd(variable, null);
            await ResolveVariables();
        }

        public async Task UpdateVariable(Models.Variable variable, string value, bool silent = false)
        {
            Console.WriteLine($"value before: {Variables.Keys.First(v => v.Name == variable.Name).Value}");
            Variables.Keys.First(v => v.Name == variable.Name).Value = value;
            if (!silent)
            {
                await ResolveVariables();
            }
            Console.WriteLine($"value after: {Variables.Keys.First(v => v.Name == variable.Name).Value}");
        }

        public async Task ResolveVariables()
        {
            foreach (var variable in Variables)
            {
                Variables[variable.Key] = await ResolveVariableStringValue(variable.Key.Value.Tokenized());
            }

            _variableEvents.OnVariablesUpdated();
        }

        public async Task<double> ResolveVariableStringValue(string input)
        {
            logger.LogInformation("Resolving: {VariableValue}", input);
            input = await ProcessInputString(input.Tokenized());
            Console.WriteLine($"processing input: {input}");
            var results = (double)((Entity)input).EvalNumerical();
            logger.LogInformation("Results: {Results}", results);
            return results;
        }

        public async Task<string> ProcessInputString(string input)
        {
            input = randomOperation.Process(input);

            foreach (var variable in Variables)
            {
                if (input.Tokenized().Contains(variable.Key.Name.Tokenized()))
                {
                    logger.LogInformation("Replacing ({VariableName})", variable.Key.Name);
                    input = input.Replace(variable.Key.Name.Tokenized(), variable.Value != null ? variable.Value.Tokenized() : (await ResolveVariableStringValue(variable.Key.Value)).Tokenized());
                }
            }

            logger.LogInformation("Resolved input: ({ResolvedVariableValue})", input);

            return input;
        }

        public async Task<double?> GetValue(string name)
        {
            var variable = Variables.FirstOrDefault(v => v.Key.Name == name);
            if (variable.Key == null)
                return null;

            return variable.Value ?? await ResolveVariableStringValue(variable.Key.Value);
        }

        public Models.Variable GetVariableByName(string name)
        {
            return Variables.FirstOrDefault(v => v.Key.Name == name).Key;
        }
    }
}
