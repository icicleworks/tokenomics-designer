﻿using Blazored.LocalStorage;
using System.Text.Json;
using Tokenomics.Designer.Events;
using Tokenomics.Designer.Models;

namespace Tokenomics.Designer.Services
{
    public class BackupManager
    {
        private readonly ILocalStorageService _localStorage;
        private readonly ChartsManager _chartsManager;
        private readonly VariablesManager _variablesManager;
        private readonly SimulatorManager _simulatorManager;

        public BackupManager(ILocalStorageService localStorage, ChartsManager chartsManager, VariablesManager variablesManager, SimulatorManager simulatorManager, ChartEvents chartEvents)
        {
            this._localStorage = localStorage;
            this._chartsManager = chartsManager;
            this._variablesManager = variablesManager;
            this._simulatorManager = simulatorManager;

            chartEvents.ChartsUpdated += async (s, e) => await SaveScene("current");
        }

        public async Task<string> SaveScene(string name)
        {
            var scene = new Scene()
            {
                Name = $"BACKUP:{name}",
                Charts = _chartsManager.Charts,
                Variables = _variablesManager.Variables.Select(v => v.Key).ToList(),
                SimulatorSettings = _simulatorManager.Settings,
                SimulatorFunctions = _simulatorManager.Functions,
            };

            await _localStorage.SetItemAsync<Scene>($"BACKUP:{name}", scene);
            return await _localStorage.GetItemAsStringAsync(name);
        }

        public async Task<string> GetSceneJsonValue(string name)
        {
            return await _localStorage.GetItemAsStringAsync($"BACKUP:{name}");
        }

        public async Task<Scene> GetSceneValue(string name)
        {
            return await _localStorage.GetItemAsync<Scene>($"BACKUP:{name}");
        }

        public async Task LoadScene(string name, bool silent = false)
        {
            Console.WriteLine($"State {{{name}}} loading");
            var scene = await _localStorage.GetItemAsync<Scene>($"BACKUP:{name}");
            _chartsManager.Charts.Clear();
            _variablesManager.Variables.Clear();
            _simulatorManager.Functions.Clear();

            _chartsManager.Charts.AddRange(scene.Charts);

            foreach (var variable in scene.Variables)
            {
                _variablesManager.Variables.Add(variable, null);
            }
            if(!silent)
                await _variablesManager.ResolveVariables();

            _simulatorManager.Settings = scene.SimulatorSettings;
            _simulatorManager.Functions = scene.SimulatorFunctions;
            Console.WriteLine($"State {{{name}}} loaded");
        }

        public async Task<IEnumerable<string>> GetScenes()
        {
            var keys = await _localStorage.KeysAsync();

            return keys.Where(key => key.StartsWith("BACKUP:")).Select(key => key.Replace("BACKUP:", ""));
        }
    }
}
