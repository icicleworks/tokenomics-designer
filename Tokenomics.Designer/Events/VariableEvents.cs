﻿using Tokenomics.Designer.Models;

namespace Tokenomics.Designer.Events
{

    public class VariableEvents
    {
        public event EventHandler VariablesUpdated;

        public void OnVariablesUpdated()
        {
            VariablesUpdated?.Invoke(this, new());
            Console.WriteLine("OnVariable updated");
        }
    }
}
