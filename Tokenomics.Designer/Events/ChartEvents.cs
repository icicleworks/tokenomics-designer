﻿namespace Tokenomics.Designer.Events
{
    public class ChartEvents
    {
        public event EventHandler ChartsUpdated;

        public void OnChartsUpdated()
        {
            ChartsUpdated?.Invoke(this, new());
        }
    }
}
