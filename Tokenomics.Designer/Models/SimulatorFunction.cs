﻿namespace Tokenomics.Designer.Models
{
    public class SimulatorFunction
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; } = true;
        public List<SimulatorInstruction> Instructions { get; set; } = new List<SimulatorInstruction>() { new()};
    }
}
