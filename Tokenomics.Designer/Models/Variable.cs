﻿namespace Tokenomics.Designer.Models
{

    public enum VariableUiControl
    {
        Text, Range
    }

    public class Variable
    {
        public string Name { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;
        public VariableUiControl VariableUiControl { get; set; }
    }
}
