﻿namespace Tokenomics.Designer.Models
{
    public class SimulatorSettings
    {
        public int Iteration { get; set; } = 0;
        public int IterateBy { get; set; } = 1;
        public int Iterations { get; set; } = 1;
        /// <summary>
        /// timeout between iterations in miliseconds
        /// </summary>
        public int IterationTime { get; set; } = 10;
    }
}
