﻿namespace Tokenomics.Designer.Models
{
    public class Scene
    {
        public string Name { get; set; }
        public List<BaseChart> Charts { get; set; }
        public List<Variable> Variables { get; set; }
        public SimulatorSettings SimulatorSettings { get; set; }
        public List<SimulatorFunction> SimulatorFunctions { get; set; }
    }
}
