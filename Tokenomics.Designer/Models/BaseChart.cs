﻿using AngouriMath;
using Tokenomics.Designer.Extensions;

namespace Tokenomics.Designer.Models
{

    public enum ChartType
    {
        Line, Bar, Pie
    }

    public class BaseChart
    {
        public string Name { get; set; }
        public List<string> Values { get; set; } = new();
        public List<string> Keys { get; set; } = new();
        public List<List<KeyValuePair<object, object>>> Series { get; set; } = new();
        public List<string> SeriesNames { get; set; } = new();
        public ChartType Type { get; set; }


        public string GetFxString(int index, double? x)
        {
            return Values[index].Replace("x".Tokenized(), x?.Tokenized() ?? "x".Tokenized());
        }

        public void AddValue(int index, KeyValuePair<object, object> value)
        {
            Series[index].Add(value);
        }

        public void ResetValues()
        {
            Series.ForEach(s => s.Clear());
        }
    }
}
