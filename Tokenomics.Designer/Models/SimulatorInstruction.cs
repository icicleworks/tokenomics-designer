﻿using Tokenomics.Designer.Extensions;

namespace Tokenomics.Designer.Models
{
    public class SimulatorInstruction
    {
        public string VariableName { get; set; } 
        public string Instruction { get; set; }
        public string Condition { get; set; }

        public string GetInstruction(int iteration)
        {
            return Instruction.Replace("i".Tokenized(), iteration.ToString().Tokenized());
        }

    }
}
