﻿namespace Tokenomics.Designer.Models
{
    public class Tokenomic
    {
        public List<Variable> Variables { get; set; } = new();
        public List<BaseChart> Graphs { get; set; } = new();
    }
}
